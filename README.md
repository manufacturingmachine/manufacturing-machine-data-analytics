# Run in Docker
* use -d flag to run in background

`> docker-compose up`

* Tear down

`> docker-compose down`

* To re-build

`> docker-compose build`

